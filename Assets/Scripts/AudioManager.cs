﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [SerializeField]
    private AudioSource _bgm;
    [SerializeField]
    private AudioSource[] _sfx;
    [SerializeField]
    private AudioClip[] _sfxLibrary;
    [SerializeField]
    private AudioClip[] _bgmLibrary;


    public void PlaySFX(int value) {
        foreach (AudioSource sfx in _sfx) {
            if (!sfx.isPlaying) {
                sfx.clip = _sfxLibrary[value];
                sfx.Play();
                return;
            }
        }
    }

    public void ChangeBGM(int value) {
        _bgm.Stop();
        _bgm.clip = _bgmLibrary[value];
        _bgm.Play();
    }

}
