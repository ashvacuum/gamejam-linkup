﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class Odometer : MonoBehaviour
{
    
    private Image _odometer;
    [SerializeField]
    private float _min;    
    [SerializeField]
    private float _max;

    public void SetOdometerValues(float min, float max) {
        _min = min;
        _max = max - min;
    }

    public void AdjustValue(float value) {
        //Debug.LogFormat("Value {0}, Min: {1}, Max: {2}", value-_min, _min, _max);
        _odometer.fillAmount = Mathf.Clamp(value - _min, 0, _max)/_max;
    }

    private void Start() {
        _odometer = GetComponent<Image>();
        _odometer.fillAmount = 0;
    }
}
