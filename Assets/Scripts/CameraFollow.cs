﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    [SerializeField]private float _timeToMove = 0.1f; 
    private Transform _target;
    
    // Start is called before the first frame update
    Vector3 _velocity;
    void Start()
    {
        _target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, new Vector3(_target.position.x, _target.position.y, -10f), _timeToMove);
        //Debug.Log("CameraFollow: " + _velocity);
    }
}
