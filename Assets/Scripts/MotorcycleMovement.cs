﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MotorcycleMovement : MonoBehaviour
{
    [System.Serializable]
    private struct MotorcycleEngine {
        public float Speed { get { return m_Speed; } }
        public float TrailEmitLimit { get { return m_trailEmitLimit; } }
        public float MaxSpeed { get { return m_maxSpeed; } }
        public float SpeedBonus { get { return m_SpeedBonus; } }

        [SerializeField] private float m_Speed;
        [SerializeField] private float m_trailEmitLimit;
        [SerializeField] private float m_maxSpeed;
        [SerializeField] private float m_SpeedBonus;
    }

    [SerializeField] private MotorcycleEngine[] _limits;
    
    [SerializeField] private float _turnSpeed = 5f;
    [Range(0, 1)]
    [SerializeField] private float _dragSlip = 0.90f;
    [Range(0, 1)]
    [SerializeField] private float _dragStick = 0.1f;

    [SerializeField] private float _minPerfectShift = 0.8f;
    [SerializeField] private float _maxPerfectShift = 0.9f;
    
    [SerializeField] private float _maxStickVelocity = 2.5f;
    [SerializeField] private float _minSlipVelocity = 1.5f;
    [SerializeField] private Transform _targetForward;

    [SerializeField] private Odometer _accelerationBar;
    [SerializeField] private Text _speedDisplay;
    [SerializeField] private Text _gearDisplay;
    [SerializeField] private Speedometer _meter;
    private int _gear = 0;
    private Rigidbody2D _rb2d;
    private TrailRenderer _trail;
    bool _isPerfectShift = false;
    bool _canPlay = false;
    [SerializeField]
    float maxSpeedPerGear = 0;

    private SoundManagement _soundMgr;
    // Start is called before the first frame update
    void Start()
    {
        GameManager.GameStarted += EnableControls;
        _soundMgr = GetComponent<SoundManagement>();
        _trail = GetComponentInChildren<TrailRenderer>();
        _rb2d = GetComponent<Rigidbody2D>();
        _accelerationBar.SetOdometerValues(0, _limits[_gear].MaxSpeed);
    }



    // Update is called once per frame
    void FixedUpdate()
    {
        if (!_canPlay) return;
        Movement();
        GearShift();
        AutomatedGearDecrease();
    }

    void EnableControls() {
        _canPlay = true;
    }

    void Movement() {

        float dragValue = RightVelocity().magnitude < _maxStickVelocity ? _dragSlip : _dragStick;        
        _trail.emitting = RightVelocity().magnitude > _limits[_gear].TrailEmitLimit;        
        _rb2d.velocity = ForwardVelocity() + RightVelocity() * dragValue;
        //TODO replace code here with speedometer
        _speedDisplay.text = Mathf.RoundToInt(_rb2d.velocity.sqrMagnitude).ToString();
        _meter.SetRotationViaSpeed(_rb2d.velocity.sqrMagnitude);

        if(Input.GetAxis("Vertical") == 0) {
            //PlayIdleMusic
        }
        if (Input.GetButton("Vertical")) {
            _rb2d.AddForce(Input.GetAxis("Vertical") < 0 ? -transform.up* _limits[0].Speed: transform.up * (_isPerfectShift ? _limits[_gear].Speed + _limits[_gear].MaxSpeed : _limits[_gear].Speed));
        }

        _accelerationBar.AdjustValue(_rb2d.velocity.magnitude);
        
        maxSpeedPerGear = Mathf.Max(maxSpeedPerGear, _rb2d.velocity.magnitude);

        float tf = Mathf.Lerp(0, _turnSpeed, _rb2d.velocity.magnitude / 10);
        _rb2d.angularVelocity = -Input.GetAxis("Horizontal") * tf;
        
    }

    void AutomatedGearDecrease() {
        if (_gear == 0) return;
        if (_rb2d.velocity.magnitude < _limits[Mathf.Clamp(_gear + -1, 0, _limits.Length - 1)].MaxSpeed * 0.8f) {
            _gear = Mathf.Clamp(_gear + -1, 0, _limits.Length - 1);
        }
        GearDisplayChange();
    }

    void GearShift() {
        if (Input.GetButtonDown("GearShift")) {
            if(_rb2d.velocity.magnitude >= _limits[_gear].MaxSpeed * _minPerfectShift && _rb2d.velocity.magnitude < _limits[_gear].MaxSpeed * _maxPerfectShift) {
                _isPerfectShift = true;
            } else {
                _isPerfectShift = false;
            }

            
            _gear = Mathf.Clamp(_gear + (int)Input.GetAxis("GearShift"), 0, _limits.Length - 1);
            GearDisplayChange();
            //run event for change gear
            Debug.LogFormat("Gear: {0}, Perfect Shift: {1}", _gear, _isPerfectShift);
        }
        _gearDisplay.text = (_gear+ 1).ToString();
    }

    void GearDisplayChange() {
        if (_gear > 0) {
            _accelerationBar.SetOdometerValues(_limits[_gear - 1].MaxSpeed, _limits[_gear].MaxSpeed);
        } else {
            _accelerationBar.SetOdometerValues(0, _limits[_gear].MaxSpeed);
        }
    }

    
    
    Vector2 ForwardVelocity() {        
        return transform.up * Vector2.Dot(_rb2d.velocity, transform.up);
    }


    Vector2 RightVelocity() {
        return transform.right * Vector2.Dot(_rb2d.velocity, transform.right);
    }
}
