﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxCollider : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision) {
        if(collision.tag == "Player") {
            FindObjectOfType<GameManager>().LapCompleted();
            ResetLap();                        
        }
    }

    private void Start() {
        ResetLap();
    }

    void ResetLap() {
        GetComponent<BoxCollider2D>().enabled = false;        
        StartCoroutine(UnlockLap());
    }

    IEnumerator UnlockLap() {
        yield return new WaitForSeconds(15f);
        GetComponent<BoxCollider2D>().enabled = true;
    }
}
