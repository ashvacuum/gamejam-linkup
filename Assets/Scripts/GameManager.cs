﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Text;
using TMPro;
using PlayFab;
using PlayFab.ClientModels;

public class GameManager : MonoBehaviour
{
    #region Game Specific Variables
    [SerializeField]
    private int _countdown = 3;
    [SerializeField]
    private TextMeshProUGUI _tmproGUI;

    [SerializeField]
    private TextMeshProUGUI _lapTimeDisplay;
    [SerializeField]
    private float startTime;

    [SerializeField]
    private GameObject _tutorialScreen;
    [SerializeField]
    private GameObject _pauseScreen;
    [SerializeField]
    private GameObject _mainScreen;
    [SerializeField]
    private List<GameObject> _menuItems;

    private List<float> _laptimes;
    private static bool _hasStarted = false;
    public static bool hasStarted { get { return _hasStarted; } }
    public static event Action GameStarted;
    public static event Action LapFinished;

    private int _laps;
    private int _maxLaps = 3;

    private StringBuilder sb = new StringBuilder();
    #endregion

    #region Playfab Variables
    [Header("Playfab Settings")]
    [SerializeField] private GameObject _loginScreen;
    public static bool isLoggedIn { get { return _isLoggedIn; } }
    [SerializeField] private static bool _isLoggedIn;
    [SerializeField] private Toggle _rememberMe;

    [SerializeField] private TMP_InputField _user;    
    [SerializeField] private TextMeshProUGUI _error;

    [SerializeField] private LeaderboardsDisplay _display;

    // Accessbility for PlayFab ID & Session Tickets
    public static string PlayFabId { get { return _playFabId; } }
    private static string _playFabId;

    public static string SessionTicket { get { return _sessionTicket; } }
    private static string _sessionTicket;


    private const string USERCODE = "USER";
    private const string PASSCODE = "PASS";
    [SerializeField]
    private GetPlayerCombinedInfoRequestParams _params;

    /// <summary>
    /// Logs in the user , to be user for login button
    /// </summary>
    public void Login() {
        _error.text = "";
        Debug.Log("Logging in user via Log in button");
        if (!string.IsNullOrEmpty(_user.text)) {

            PlayFabClientAPI.LoginWithPlayFab(
                new LoginWithPlayFabRequest() {
                    TitleId = PlayFabSettings.TitleId,
                    Username = _user.text,
                    Password = SystemInfo.deviceUniqueIdentifier,
                    InfoRequestParameters = _params
                },

                // Success
                (LoginResult result) => {
                    //Store identity and session
                    _playFabId = result.PlayFabId;
                    _sessionTicket = result.SessionTicket;
                    _isLoggedIn = true;
                    Debug.Log("user has logged in");
                    if(_rememberMe.isOn) {
                        PlayerPrefs.SetString(USERCODE, _user.text);
                        
                    } else {
                        RemoveUserAndPassData();
                    }
                    _mainScreen.SetActive(true);
                    _loginScreen.SetActive(false);
                    //Close window, open play screen and load leaderboards data
                },

                // Failure
                (PlayFabError error) => {
                    _error.text = error.GenerateErrorReport();
                });
            ClearUserAndPass();
            return;
        } else {
            _error.text = "Username/Password must not be empty";
        }
    }

    /// <summary>
    /// Logs in user by default
    /// </summary>
    /// <param name="user">username from player prefs</param>
    /// <param name="pass">password from player prefs</param>
    private void Login(string user) {
        _error.text = "";
        Debug.Log("Logging in user via remember me");
        if (!string.IsNullOrEmpty(_user.text)) {

            PlayFabClientAPI.LoginWithPlayFab(
                new LoginWithPlayFabRequest() {
                    TitleId = PlayFabSettings.TitleId,
                    Username = _user.text,
                    Password = SystemInfo.deviceUniqueIdentifier,
                    InfoRequestParameters = _params
                },

                // Success
                (LoginResult result) => {
                    //Store identity and session
                    _playFabId = result.PlayFabId;
                    _sessionTicket = result.SessionTicket;
                    _isLoggedIn = true;                    
                    if (_rememberMe.isOn) {
                        PlayerPrefs.SetString(USERCODE, _user.text);
                        
                    } else {
                        RemoveUserAndPassData();
                    }
                    _mainScreen.SetActive(true);
                    _loginScreen.SetActive(false);
                    //Close window, open play screen and load leaderboards data
                },

                // Failure
                (PlayFabError error) => {
                    _error.text = error.GenerateErrorReport();
                });
            ClearUserAndPass();
            return;
        }
    }

    void ClearUserAndPass() {
        _user.text = "";        
    }

    public void RemoveUserAndPassData() {
        PlayerPrefs.DeleteKey(USERCODE);
        
    }

    public void Register() {
        
        if (!string.IsNullOrEmpty(_user.text)) {
            PlayFabClientAPI.RegisterPlayFabUser(
            new RegisterPlayFabUserRequest {
                TitleId = PlayFabSettings.TitleId,
                Username = _user.text,
                Password = SystemInfo.deviceUniqueIdentifier,
                InfoRequestParameters = _params,
                DisplayName = _user.text,
                RequireBothUsernameAndEmail = false
            },
            (RegisterPlayFabUserResult result) => {
                //Store identity and session
                _playFabId = result.PlayFabId;
                _sessionTicket = result.SessionTicket;
                _isLoggedIn = true;
                if (_rememberMe.isOn) {
                    PlayerPrefs.SetString(USERCODE, _user.text);
                } else {
                    RemoveUserAndPassData();
                }
                _mainScreen.SetActive(true);
                _loginScreen.SetActive(false);
                //Close window, open play screen and load leaderboards data
            },

                // Failure
                (PlayFabError error) => {
                    _error.text = error.GenerateErrorReport();
                }

            );

        } else {
            _error.text = "Username/Password should not be empty";
        }

        
        ClearUserAndPass();
    }

    public void RegisterLeaderboard(float value) {
        if (!_isLoggedIn) return;
        Debug.Log(Mathf.RoundToInt(value));
        Debug.Log(value);
        PlayFabClientAPI.WritePlayerEvent(new WriteClientPlayerEventRequest {
            EventName = "update_statistic",            
            Body = new Dictionary<string, object>
                {
                    { "stat_name", "TimeTrials" },
                    { "value", Mathf.RoundToInt(value) }
                }
        },
        (WriteEventResponse request) => {
            Debug.Log("Success Writing to leaderboards");
        },
        (PlayFabError error) => {
            Debug.Log("Error");
        });
    }

    #endregion

    #region Monobehavior Functions
    // Start is called before the first frame update
    void Start()
    {
        _laps = 1;
        CloseAllMenus();
        _loginScreen.SetActive(true);
        if (!string.IsNullOrEmpty(PlayerPrefs.GetString(USERCODE))) {
            Login(PlayerPrefs.GetString(USERCODE));
        }
        _laptimes = new List<float>();
        
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Escape) && _hasStarted) {
            PauseGame();
        }
    }
    #endregion
    
    #region Private Methods
    void CloseAllMenus() {
        foreach(GameObject g in _menuItems) {
            g.SetActive(false);
        }
    }

    IEnumerator Countdown() {        
        Time.timeScale = 1;
        if (_hasStarted) yield break;
        for(int i = _countdown; i > 0; i--) {
            yield return new WaitForSeconds(1f);
            _tmproGUI.text = i.ToString();
        }
        _tmproGUI.text = "START";
        startTime = Time.time;
        GameStarted?.Invoke();
        _hasStarted = true;
        yield return new WaitForSeconds(0.2f);
        _tmproGUI.text = "";
    }
    #endregion

    #region Game Manager Callable Functions
    public void LapCompleted() {
        Debug.Log("Working");
        float time = Time.time - startTime;
        _laptimes.Add(time);
        RegisterLeaderboard((time/60f) * 60 + (time % 60));
        startTime = Time.time;
        //TODO add display on here
        sb.Clear();
        _laptimes.ForEach((item) => {
            sb.Append("Lap").Append("#").Append(_laps).Append(": ").Append(Mathf.RoundToInt(item / 60f)).Append(":").Append(Mathf.RoundToInt(item % 60)).AppendLine();
        });
        _lapTimeDisplay.text = "";
        _lapTimeDisplay.text = sb.ToString();
        if (_laps + 1 > _maxLaps) {
            ResetGame();
            return;
        }
        _laps++;
    }

    public void StartGame() {
        ShowTutorial();
    }

    public void PauseGame() {
        CloseAllMenus();
        _pauseScreen.SetActive(true);
        Time.timeScale = 0;
    }

    public void ResetGame() {
        SceneManager.LoadScene(0);
    }

    public void ExitGame() {
        Application.Quit();
    }

    public void ShowTutorial() {
        CloseAllMenus();
        _tutorialScreen.SetActive(true);
    }

    public void CloseTutorial() {
        StartCoroutine(Countdown());
        CloseAllMenus();
    }
    #endregion
    
}

