﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Speedometer : MonoBehaviour
{

    [SerializeField] private Transform _needle;    

    public void SetRotationViaSpeed(float value) {
        if(value > 0) {
            _needle.rotation = Quaternion.Euler(new Vector3(0, 0, -value));
        } else {
            _needle.rotation = Quaternion.Euler(Vector3.zero);
        }
    }
}
