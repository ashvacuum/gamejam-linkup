﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using TMPro;
using System.Text;

public class LeaderboardsDisplay : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI _leaderboardsText;
    private StringBuilder _sb;
    
    private void OnEnable() {
        RefreshLeaderboards();        
    }

    void DisplayLeaderboards(List<PlayerLeaderboardEntry> entries) {
        if(entries.Count == 0) {
            if (_leaderboardsText != null) {
                _leaderboardsText.text = "No Entries Found";
            }
            return;
        }
        _sb = new StringBuilder();
        entries.ForEach((entry) => {
            _sb.Append(entry.Position + 1).Append(" ").Append(entry.DisplayName).Append(" ")
            .Append(Mathf.RoundToInt(entry.StatValue / 60)).Append(":")
            .Append(Mathf.RoundToInt(entry.StatValue % 60)).AppendLine();
        });

        if (_leaderboardsText != null) {
            _leaderboardsText.text = _sb.ToString();
        }
    }

    public void RefreshLeaderboards() {
        if (_leaderboardsText != null) {
            _leaderboardsText.text = "Loading";
        }
        if (!GameManager.isLoggedIn) {
            //launch login menu
            if (_leaderboardsText != null) {
                _leaderboardsText.text = "You are not logged in to view leaderboards";
            }
        } else {
            PlayFabClientAPI.GetLeaderboard(
                new GetLeaderboardRequest {
                    StartPosition = 0,
                    StatisticName = "TimeTrials"
                },
                (GetLeaderboardResult result) => {
                    DisplayLeaderboards(result.Leaderboard);
                },
                (PlayFabError error) => {
                    _leaderboardsText.text = error.GenerateErrorReport();
                }
            );

        }

    }
}
